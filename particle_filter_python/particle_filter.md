# Montecarlo Localization (MCL) using a Particle Filter (PF)

The problem that we will try to solve in this lab is the following: to localize a robot having only its initial position and orientation, the odometry (i.e., linear $v$ and angular $w$ velocities) and few range measurement that the robot takes from time to time with respect to the walls around it (check next figure to see how the ranges are distributed).

![](./media/lasers.png)

You have to implement a map-based localization algorithm. We provide you the map in which the robot is moving around as a grid-map (see next figure). In the grid-map, white pixels represent occupied cells and black pixels represent free or unknown cells. This grid-map has been created combining the laser ranges and the ground truth navigation (i.e., GPS data) for this dataset. The image has 125 x 100 pixels and represents a map of 25 x 20 m (i.e., each pixel represents a 0.2 x 0.2 m cell). 

![](./media/map.png)

The algorithm that you have to use is a particle filter (PF), a kind of Montecarlo localization (MCL) algorithm. The standard form looks as follows:

```python    
    def Algorithm_MCL(Xt1 , ut , zt , m):
        ^Xt = Xt = ∅
        for m in M:
            xt^m = sample_motion_model(ut , xt1^m)
            wt^m = measurement_model(zt, xt^m, m)
            ^Xt = ^Xt + {xt^m, wt^m}
        for m in M:
            draw i with probability ∝ wt^i
            add xt^i to Xt
        return Xt
```

The algorithm has three main sections: predict (*sample_motion_model*), update (*measurement_model*) and re-sample (*draw with probability*). You will have to implement these parts inside a class named `ParticleFilter`. Here is a [video](https://www.youtube.com/watch?v=NrzmH_yerBU&list=PLn8PRpmsu08rLRGrnF-S6TyGrmcA2X7kg&index=2) explaining the particle filter concept.

## Exercise 1: ParticleFilter class

Create a `ParticleFilter` class following this skeleton:

```python
from utils import wrap_angle
from utils import GridMap
...


class ParticleFilter:
    def __init__(self, num_particles, grid_map, odom_sigma, rng_sigma, 
                 p_init, h_init, beam_angles, max_rng):
        # Save constructor values
        ...
        
        # Initialize particles and weights
        self.px = ... # x for each particle
        self.py = ... # y for each particle
        self.h = ...  # heading for each particle
        self.w = ...  # weight for eaxh particle
        ...
        
        # Others
        self.moving = False
        self.n_eff = None
```

where:

* **num_particles**: Number of particles.
* **grid_map**: Don't worry about this object! It is used to simulate the ranges that a robot should measure according to its position and orientation. We will provide it!
* **odom_sigma**: Odometry noise [$\sigma_v$, $\sigma_w$]
* **rng_sigma**: Range measurement noise $\sigma_r$
* **p_init**: Approximated initial position for the robot [$p_x$, $p_y$]
* **h_init**: Approximated initial orientation for the robot $\theta$
* **beam_angles**: list of angles (defined respect to the vehicle frame) in which the range sensors are pointed (see first figure).
* **max_range**: Maximum range value per measurement.

In the constructor you have to save all these parameters and initialize all the particles in a random position between 0 and 1 meters around `p_init` and with a heading equal to `h_init` plus/minus a random value between -10 and 10 degrees.

Create a file with the `main` function and initialize the `ParticleFilter` class with the following values. Then, load the trajectory ground truth and the odometry data files and compute the vehicle position if only the odometry was available (create a function for this). Plot the resulting odometry position together with the ground truth data. Then plot all the particles already initialized in the class constructor. 

```python
...

if __name__ == '__main__':

    # Loading GridMap class (min x, max x, min y max y, map filename)
    grid_map = GridMap(-15, 10, -10, 10, "map.png")

    # Define laser angles (repect vehicle heading)
    laser_angles = [-2.095, -1.0475, 0, 1.0475, 2.095]

    # Initialize particle filter class
    pf = ParticleFilter(100, grid_map, (0.075, 0.05), 0.2, 
                        (4.0, 0.0), math.pi/2, laser_angles, 10.0)
    # Load datafiles
    ...

    # Compute trajectory from odometry
    ...

    # Plot ground truth and odometry trajectories as well as particles
    ...
```



## Exercise 2: Prediction

Create the *predict* method in the `ParticleFilter` class following this skeleton:

```python
def predict(self, odom): # odom = [inc_t, v, w]
    if odom[0] == 0 or (odom[1] == 0 and odom[2] == 0):
        self.moving = False
    else:
        # Apply motion to each particle
        ...
        self.moving = True
```

Move each particle according to the odometry values. **IMPORTANT!** Add independent noise (according to *odom_sigma*) to each particle.

In the `main`, complete the following code an execute it.

```python
# Initialize particle filter
...
pf = ParticleFilter(100, grid_map, (0.075, 0.05), 0.2, (4.0, 0.0), math.pi/2,
                    laser_angles, 10.0)
#Load odometry data file
odom_data = ...

last_time = odom_data.iloc[0].time
for index, row in odom_data.iterrows():
    ...
    odom = ... # [inc_t, v, w]
    pf.predict(odom)
    
    # Save the current state as an image
    if index % 10 == 0:
    	# Plot ground truth and particles
   		...
		# save image to disk
        plt.savefig("pf_{:04d}.png".format(index))
        plt.close()
```

Create a GIF with the images generated. You should see that the particles follow the ground truth trajectory (the odometry is good enough) but they are more disperse every time.

![](./media/prediction.gif)



## Exercise 3: Update

Now it is time to apply the updates using the range information provided by the laser sensors. For this step, we need to *simulate* the ranges that each particle should get in a particular position ($\mu$) with respect to the known map and compare them with the ranges ($x$) that the robot has really measured (*laser_scan* data file). In the PF, instead of analyzing all possible robot positions (that will take infinite time) only those positions in which a particle exists will be examined. The result of comparing the real measures with those obtained in the particle position will be used to compute the particle weight. The comparison must be done with a Gaussian:
$$
w = \frac{1}{\sigma \sqrt{2 \pi}} \exp \left[ - \frac{(x - \mu)^2}{2 \sigma^2} \right]
$$
being $x$ the measured value , $\mu$ the expected value and $\sigma$ the uncertainty of the measurement.
To *simulate* the ranges measured form each particle according to a given grid-map, use the `calculate_ray_value` method.

Add the following methods in the `ParticleFilter` class following this skeleton:

```python
# Given a position and orientation, it returns the range measurements that a vehicle at 
# this pose should get acoording to the grid_map, the range sensor angles and the 
# maximum range  
def calculate_ray_values(self, px, py, h):
    # Use the method check_ray((particle_x, particle_y), range_angle, max_range) from 
    # the GridMap object to obtain the range of each laser sensor
    ...
    return # a NumPy array with the n ranges for the particle in position (px, py, h)

# Updates the weight of each particle
def update(self, ranges):
    # For each particle
    for i in range(self.num_particles):
        # Estimated ranges to map from this particle
        map_ranges = self.calculate_ray_values(...)
        # Compute weight
        ...
        # Combine weight with previous
        ...
    # Normalize weights so as their sum is 1.
    self.w = ...
    # Compute efficient number
    self.n_eff = ...

# To estimate the current position and heading averaging all particles 
# according to its weight 
def get_mean_particle(self):
        mean_x = ...
        mean_y = ...
        mean_h = ...     
        return np.array([mean_x, mean_y, mean_h])
```

In the `main` file, apply the predictions as well as the updates whenever possible.

```python
# Initialize particle filter
...

px = list()
py = list()
laser_idx = 0
last_time = odom_data.iloc[0].time
for index, row in odom_data.iterrows():
    ...
    odom = ... # [inc_t, v, w]
    
    # Predict motion according to odometry
    pf.predict(odom)
    
    # If there is range data apply update
    if laser_idx < len(laser_data) and last_time > laser_data.iloc[laser_idx].time:
        ranges = laser_data.iloc[laser_idx].values[1:]
        pf.update(ranges)
        laser_idx += 1
        
    # Get averaged pose
    pose = pf.get_mean_particle()
    px.append(pose[0])
    py.append(pose[1])
    
    # Save image
    if (index % 10) == 0:
        # Plot ground truth, estimated position and particles. Save image to disk.
        ...
```

At first sight, it seems that nothing has change and in part it is true: we see the same particles (with different noise) moving around. However, now each particle has a weight associated that allows us to estimate the center of masses of this cloud of particles.

![](./media/no_resample.gif)

## Exercise 4: Re-sampling

Now that each particle weight is correctly estimated lets re-sample those particles with less weight. Program a *re-sampling* method in the `ParticleFilter` class following the next skeleton. You can use any of the algorithms presented in the course.

```python
 def resample(self):
     # If conditions for resampling are meet   
     if ...    
        # Resample particles
        ...
```

Execute again the particle filter algorithm but adding a call to the *resampling* algorithm after each *update* in the `main` file. You will see that now all the particles stay together and the final solution is closer to the ground truth.

![](./media/pf.gif)

## Deliverable

Create a folder containing:

* **report**: PDF file with no more than 4 pages.  

* **src**: source files `utils.py`, `particle_filter.py` and `main_pf.py`.
* **data**: (as given) data files `map.png`, and all *.csv files. Do not change any name!
* **media**: 3 animations (.gif, .mp4, ...) with the result of your implementation.

Remember that you don't have to use ROS for this lab, just python! For your convenience, you can use the following libraries:

* **numpy**: for vector operations
* **pandas**: to load and manipulate CSV files
* **matplotlib**: for plotting purposes  

