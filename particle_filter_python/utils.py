import math
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt


def wrap_angle(ang):
    if isinstance(ang, np.ndarray):
        ang[ang > np.pi] -= 2 * np.pi
        return ang
    else:
        return ang + (2.0 * math.pi * math.floor((math.pi - ang) / (2.0 * math.pi)))


def compute_position_error(gt_time, gt_x, gt_y, data_time, data_x, data_y):
    idx = 1
    error = np.array([])
    for t, x, y in zip(gt_time, gt_x, gt_y):
        if idx < len(data_time) and t > data_time[idx]:
            e = np.linalg.norm(np.array([x, y]) - 
                               np.array([data_x[idx], data_y[idx]]))
            idx += 1
            error = np.concatenate((error, np.array([e])))
    return error


class GridMap:
    def __init__(self, min_x, max_x, min_y, max_y, filename):
        self.min_x = min_x
        self.max_x = max_x
        self.min_y = min_y
        self.max_y = max_y
        self.grid_map = mpimg.imread(filename)
        self.lx = self.grid_map.shape[1]
        self.ly = self.grid_map.shape[0]
        self.resolution_x = (self.max_x - self.min_x) / self.lx
        self.resolution_y = (self.max_y - self.min_y) / self.ly

    def get_resolution(self):
        return self.resolution_x, self.resolution_y

    def get_grid_map(self):
        return np.copy(self.grid_map)

    def point_to_coord(self, p):
        return self.ly - int((p[1] - self.min_y) / self.resolution_y), int((p[0] - self.min_x) / self.resolution_x)

    def coord_to_point(self, p):
        return (p[1] * self.resolution_x) + self.min_x, ((self.ly - p[0]) * self.resolution_y) + self.min_y

    def add_point(self, px, py):
        x, y = self.point_to_coord((px, py))
        self.grid_map[x, y] = 0.5

    def show(self):
        plt.imshow(self.grid_map)
        # plt.colorbar()
        plt.show()

    def check_ray(self, start, angle, rng):
        end = (start[0] + math.cos(angle) * rng, start[1] + math.sin(angle) * rng)
        # print("ray from {} to {}".format(start, end))
        line, cp = self.ray_casting(start, end)

        if len(line) == 0:
            return -2
        if line[-1].all() == cp.all():
            return np.linalg.norm(start - cp)
        else:
            return -1

    def ray_casting(self, start, end):
        points = list()
        collision = np.array([0, 0])
        if start[0] < self.min_x or start[0] >= self.max_x or start[1] < self.min_y or start[1] >= self.max_x:
            return points, collision

        x1, y1 = self.point_to_coord(start)
        x2, y2 = self.point_to_coord(end)
        dx = x2 - x1
        dy = y2 - y1

        # Check which dimension moves mnore
        is_steep = abs(dy) > abs(dx)
        if is_steep:  # rotate line
            x1, y1 = y1, x1
            x2, y2 = y2, x2

        dx = x2 - x1
        dy = y2 - y1

        if dx == 0:
            points.append(start)
            return points, points[-1]

        deltaerr = abs(dy / dx)
        error = 0.0
        y = y1

        # Check line direction
        iterator = 1
        if x1 > x2:
            iterator = -1

        # Algorithm itself
        for x in range(x1, x2 + 1, iterator):
            if is_steep:
                x_, y_ = y, x
            else:
                x_, y_ = x, y

            px, py = self.coord_to_point((x_, y_))
            points.append([px, py])

            # Check collision with map
            if x_ < 0 or y_ < 0 or x_ >= self.ly or y_ >= self.lx:
                # print("Out of map!")
                points = points[:-1]
                break
            elif self.grid_map[x_, y_] > 0.5:
                collision = np.array([px, py])
                break
            else:
                error += deltaerr
                if error >= 0.5:
                    y = y + np.sign(dy)
                    error -= 1.0
        return np.array(points), collision