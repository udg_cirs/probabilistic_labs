# TURTLEBOT 2 Lab

This lab assumes certain knowledge of ROS. It introduces the turtlebot2 robot available on the EPS robotics lab.
It is completed with the materials found in: [https://bitbucket.org/udg_cirs/turtlebot2_udg/](https://bitbucket.org/udg_cirs/turtlebot2_udg/src/master/)