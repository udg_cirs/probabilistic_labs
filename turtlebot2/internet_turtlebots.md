# Com poder tenir internet als turtlebots

# Problema

Inicialment, els ordinadors dels turtlebots es posaven en mode hotspot per crear un punt d'accés i permetre a altres ordinadors connectar-se. D'aquesta manera, cap ordinador de la xarxa tenia accés a internet, a no ser que algun ordinador es connectés via ethernet i compartís la xarxa.
Actualment, els turtlebots porten un router per millorar la connectivitat. Però si s'utilitza com a router per defecte, el problema persisteix: cap ordinador té accés a internet. En aquest cas, com que el portatil del turtlebot utilitza la interficie ethernet, pot utilitzar la targeta wifi per connectar-se a una xarxa (e.g., CIRS, EPS) i tenir internet. Però els ordinadors que es connectin al router no en tindran, encara que el turtlebot comparteixi, ja que el router redirigeix les communicacions externes pel port WAN.

# Solució

* Al portatil del turtlebot, anar a configuració de xarxa ethernet, crear un perfil i a la pestanya "IPv4" seleccionar Shared to other computers. A tots els turtlebots el perfil s'anomena "ShareWifi". Aquest perfil permetrà que el portatil es vegi com un router des de la interficie ethernet, amb connexió a internet a través de la interficie wifi.

* La ip per defecte (no es pot canviar gràficament) és 10.42.0.1. Per evitar conflictes de claus ssh al connectar-se a multiples robots amb un ordinador, és millor canviar les ip. S'ha definit la xarxa 10.42.x.1/24 a cada turtlebot, on x és el número de turtlebot assignat. Aquest canvi no es pot fer gràficament, per tant cal executar:
  ```bash
  $ nmcli connection modify ShareWifi +ipv4.addresses 10.42.x.1/24 
  ```
 
* Connectar el via ethernet el port WAN del router (color blau) amb el portatil del turtlebot.

* Utilitzant un altre ordinador, connectar-se al router via browser (ip:192.168.31.1, psw: kobuki2021). Anar a configuració de xarxa i a baix de tot, canviar el mode del router a repetidor via ethernet. 

* Els portatils que es connectin als turtlebots, assignar una ip estatica dins la subxarxa 10.42.x.1/24, amb gateway 10.42.x.1 i dns 10.42.x.1

* Nota: A partir d'ara, el router tindrà assignada una ip via dhcp. Si es vol accedir a la configuració del router, ha d'estar connectat al turtlebot (o un ordinador compartint wifi). Feu nmap -sP 10.42.x.1/24 per trobar la ip, i accedir-hi via browser.

* Nota 2: Podeu visualitzar la connexió via router de manera transparent: És com si us connectessiu directament al turtebot. La ip del router no es necessita per rés més que per configurar-lo.

TODO: Dins la configuració del router, crec que es pot assignar una ip estatica.

Penseu en canviar el /etc/hosts si es necessari!

Roger


