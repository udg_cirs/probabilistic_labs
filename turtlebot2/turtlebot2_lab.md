# Turtlebot lab

TurtleBot is a low-cost, personal robot kit with open-source software. It was created at Willow Garage by Melonee Wise and Tully Foote in November 2010. The TurtleBot kit consists of a mobile base, 2D/3D distance sensor, laptop computer, and the TurtleBot mounting hardware kit. For more information about the hardware and the software, please check [http://www.turtlebot.com](http://www.turtlebot.com/).

![](./turtlebot2.png)

In this lab we are going to see an introduction to the turtlebot 2 robot but also to several useful ROS tools like the ROS visualizer (RViz) and the ROS logging system (ROS bags).  

## Network configuration

In previous labs you have seen that a ROS application is composed by several *nodes* that communicate ones with others. In this lab you will see that these nodes don't even have to be in the same PC. 

Using SSH (secure Shell Protocol) we will start a *roscore* and several nodes in the turtlebot laptop while you will run other applications in your own computer to visualize the data gathered by the robot or to move it. All these nodes will use the *roscore* as a kind of naming server to *find* the other nodes.

In order to exchange messages with several computers perform the following configuration actions. Notice that your PC (base station) and the turtlebot PC should be connected in the same network.

* Check the turtlebot and base station IP addresses.

  ```bash
  $ ifconfig
  ```
* From the base station connect to the turtlebot with an ssh-client.

  ```bash
  $ ssh TURTLE_BOT_USER@TURTLEBOT_IP
  ```

* Edit the `/etc/hosts` file of each PC (turtlebot and base station) to include the MACHINE_NAME and MACHINE_IP of the other PC.

* If you don't have a public/private key on your PC create one with `ssh-keygen`, just follow the instructions. Then copy the public key from the base station to the turtlebot.

  ```bash
  $ ssh-copy-id -i ~/.ssh/YOUR_KEY.pub TURTLEBOT_USER@TURTLEBOT_MACHINE_NAME
  ```

  Repeat the process to copy the turtlebot key to the base station.

* Because the `roscore` will be started in the turtlebot PC, you need to indicate its URI (Uniform Resource Identifier) to the base station. To do it execute the following command to each terminal that you want to use to communicate with the turtlebot.

  ```bash
  $ export ROS_MASTER_URI=http://TURTLEBOT_MACHINE:11311
  ```
NOTE: you can add this line at the end of your *~/.bashrc* file so it is automatically exported in any terminal session. (Remember to bring it back to localhost when working locally).

## Roscore

Roscore is a collection of nodes and programs that are pre-requisites of a ROS-based system. You must have a roscore running in order for ROS nodes to communicate. It is launched using the roscore command.

roscore will start up:
  - a ROS Master
  - a ROS Parameter Server
  - a rosout logging node 

NOTE: If you use roslaunch, it will automatically start roscore if it detects that it is not already running (unless the --wait argument is supplied).
However, it is recommended to start roscore in a separate terminal, otherwise it can lead to strange behaviours. If you kill the first launchfile (and the core) and then restart it, nodes will not be able to connect anymore as they still rely on the old (vanished) roscore, so all ROS system needs to be re-launched.

## Bring up

Switch on the kobuki base with the switch at the side of the base. Then, bring up the robot by running the following launch through the SSH. Because you will need a different terminal for each command, you can use the `byobu` linux utility, already installed in the turtlebot laptops. `byobu` let you open multiple terminals inside a single one, and keeps the byobu session running in the background. This is a very usefull feature, since otherwise, if we lose the SSH connection (e.g., a network drop), the processes running in that session would die as well.

```bash
$ byobu # optional step (but highly recommendable)
$ roscore # optional step (but highly recomendable)
# open new byobu tab / terminal
$ roslaunch turtlebot_bringup minimal.launch --screen
```

From the base station, use the `rostopic list` command to check all the topics that are published by the turtlebot. Try to discover the utility of each topic (use `rostopic echo`, `rostopic info` and `rosmsg show` commands to do it).

Then enable the kinect sensor driver.

```bash
$ roslaunch openni_launch openni.launch
```

Check again the topics that are been published. Is the `rostopic echo` command useful to visualize the images gathered by the kinect camera?   

## RViz

ROS includes several visualization tools: `rqt_image_view` to show images, `rqt_plot` to plot graphics, `rqt_tf_tree` to visualize transformations, `rqt_bag` to show logged data... Also, there are some great open-source visualization tools (for example [PlotJuggler](https://github.com/facontidavide/PlotJuggler) is a much better alternative to `rqt_plot` and `rqt_bag`). One of the best tools it includes by default is a 3D visualizer called RViz (ROS Vizualizer).

Open a new terminal in the base station and run the RViz tool on it. Remember that to see the topics published in the turtlebot PC you need to specify the *roscore* URI to each new terminal!

```bash
$ export ROS_MASTER_URI=http://TURTLEBOT_MACHINE:11311
$ rviz
```

RViz is a very useful tool that will allow you to visualize most of the messages published by a ROS-based robot. It contains a big black panel with the 3D view (for now it should be empty). On the left is the *Displays* list, which will show any displays you have loaded (right now it should just contain the global options). Set up the following value for the *global options/fixed frame*:

```yaml
Fixed frame: odom
```

If there are more elements on the Display list remove them. On the right there are some of the other panels. You can find more information about RViz in its [user guide](http://wiki.ros.org/rviz/UserGuide).

Add the following elements in the *Displays* list:

* **Image**: 
  * topic: /camera/rgb/image_color
  * Transport Hint: Compressed
* **Odometry**: 
  * topic: /odom
  * keep: 10000
  * Covariance: Disable it

Now you should see the turtlebot camera as well as an arrow indicating its position.

## Teleoperation

Its time to move the turtlebot! To do it you have to run the following teleoperation node. If it is not installed in the base station, execute it through the SSH in the turtlebot PC.

```bash
$ roslaunch turtlebot_teleop keyboard_teleop.launch
```

Move the turtlebot around. Does the odometry messages make any sense? You can check them using the `rostopic echo` tool or visualize them in 3D with the RViz.

## Depth camera

Now lets add the depth data provided by the kinect in the RViz. Every image gathered by the kinect is transformed into a set of 3D points. This data structure is usually called Point Cloud. Add the following element in the RViz Display list:

* **PointCLoud2**:
  * topic: /camera/depth/points
  * Size: 0.05
  * Color Transformer: AxisColor

Move the turtlebot in front of someone or some corner to see if what you see makes any sense. 

The point clouds contain a lot of information and unlike the camera, they are not compressed. Therefore, to avoid collapsing the network there are several solution. The first one is limit the data send from the robot to the control PC to a fraction of the messages that are generated. You can do it with the `throttle` tool in the `topic_tools` package. Execute the following line in the turtlebot PC.

```bash
$ rosrun topic_tools throttle messages /camera/depth/points 0.5
```

Now, replace the PointCloud2 topic in the RViz to `/camera/depth/points_throttle`.

Doing that, the RViz will receive only one topic every 2 seconds decreasing the necessary network bandwidth. ROS has thousands of these small tools that can make your life easier!

**[optional]** Another solution is to simulate that the kinect in our turtlebot is a laser scanner. Laser scanners are very common sensors in industrial robots, autonomous cars, etc. They are faster, more reliable, and much expensive than a kinect! However with the `pointcloud_to_laserscan` node and a kinect we can *simulate* one of these sensors. Check the following [wiki](http://wiki.ros.org/action/fullsearch/pointcloud_to_laserscan?action=fullsearch&context=180&value=linkto%3A) to run it. You will need to define some [params](http://wiki.ros.org/roslaunch/XML/param) and [remap](http://wiki.ros.org/roslaunch/XML/remap) some topics. Visualize the resulting laser scan in the RViz as done before.

Before continuing the lab, disable the PointCloud2 displays to avoid overloading the network or the CPU.

## Log data

Logging is fundamental when working with robots. To create any robotic application it is normal to make an intensive use of recorded data to develop any new algorithm. 

ROS provides several tools to log any published message, re-play them, transform the messages to another format, etc. We are going to see 2 of these tools:

*  **rosbag**: is used to record a set of topics in a file called ROS *bag*. This tool provides also utilities to replay the bag files, compress them, apply filters... 

  ```bash
  $ rosbag 
  check       compress    decompress  filter      fix         help        info        play        record      reindex
  ```

* **rqt_bag**: is a graphical application for recording and managing bag files. They primary features are:

  - show bag message contents
  - display image messages (optionally as thumbnails on a timeline)
  - plot configurable time-series of message values
  - publish/record messages on selected topics to/from ROS
  - export messages in a time range to a new bag
  
* **PlotJuggler**: rqt_plot and rqt_bag on steroids. PlotJuggler is a graphical tool to visualize time series that is **fast**, **powerful** and **intuitive**. Some features include:
  - Simple Drag & Drop user interface
  - Load data from file (csv, rosbag, ...)
  - Connect to live streaming of data (ros topics, web sockets,...)
  - Save the visualization layout
  - Transform your data using custom functions (e.g., derivative, moving average, integral,...)
  

## Deliverable

### Teleoperation and bag file

Start the turtlebot, the kinect and the teleoperation node through SSH. Using the RViz and visualizing **only** the camera image and the odometry, teleoperate the turtlebot around the class. Perform a closed trajectory (start and stop in the same position). Investigate how to use the `rosbag` tool to record **only** the following messages:

* /odom
* /camera/rgb/image_color/compressed
* /scan

![](./rviz.png)



Using the `rqt_bag` or `PlotJuggler` application, generate plots for the

* linear velocity x (*odom.twist.twist.linear.x*)
* angular velocity z (*odom.twist.twist.angular.z*)

Create a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) file with the following elements:

* time stamp
* linear velocity
* angular velocity

Create a second CSV file with the following fields and plot the $x$ $y$ trajectory:

* time stamp
* position $x$ 
* position $y$. 
* angle $\psi$

Then create a python script that composes the vehicle position $x$, $y$, $\psi$ using only the linear velocity $v$ and angular velocity $w$, from the first CSV file that you have created. Use the following equations to do it. Store the resulting $x$, $y$ and $\psi$ in a CSV file and plot the resultin $x$ $y$ trajectory.

$$
\Delta_t = t_i - t_{i-1}\\
\psi_i = wrap\_angle(\psi_{i-1} + w_i \cdot \Delta_t)\\
x_i = x_{i-1} + v_i \cdot \Delta_t \cdot \cos(\psi_i) \\
y_i = y_{i-1} + v_i \cdot \Delta_t \cdot \sin(\psi_i)
$$

where $t_i$ is the current time and `wrap_angle()` is a function that normalizes an angle between $-\pi$ and $\pi$. Initialize $x_0$, $y_0$ and $\psi_0$ to 0.

Compare the two trajectories obtained. Are they identical? Initialize $x_0$, $y_0$ and $\psi_0$ to the values you have in the second CSV (i.e., first row). How are the trajectories now?


### Move to a waypoint

RViz allows to publish a `geometry_msgs/PoseStamped` message in the topic `/move_base_simple/goal` using a button in its interface (i.e., `2D Nav Goal`).

Create a ROS node that subscribes to this topic and generates the necessary velocity commands to drive the turtlebot to that position. 

*Note 1:* If you are using the real turtlebot, ensure that when you ask for a velocity (e.g., 0.25 m/s), you don't *jump* from 0m/s to 0.25m/s immediately. You have to create a ramp (e.g., 0.0, 0.02, 0.04, 0.06 ... 0.25) to move the robot smoothly. Publish the messages at 10Hz.

*Note 2:* If you are using the `turtlesim_node` you will found a problem: `turtlebot_sim` publishes the *turtle* position using a custom message `turtlesim/Pose` that RViz can not visualize. In order to see the simulated turtle position in RViz you can add the following code in the *callback* where you have the `turtlesim/Pose` message:

```Python
odom = Odometry()
odom.header.stamp = rospy.Time.now()
odom.header.frame_id = '/map'
odom.pose.pose.position.x = msg.x
odom.pose.pose.position.y = msg.y
odom.pose.pose.position.z = 0.0
quaternion = tf.transformations.quaternion_from_euler(0, 0, msg.theta)
odom.pose.pose.orientation.x = quaternion[0]
odom.pose.pose.orientation.y = quaternion[1]
odom.pose.pose.orientation.z = quaternion[2]
odom.pose.pose.orientation.w = quaternion[3]
self.pub_odom.publish(odom)
```

This code transforms a custom `turtlesim/Pose` message into a standard `nav_msgs/Odometry` message that can be visualized in RViz. You will need to import

```Python
from nav_msgs.msg import Odometry
import tf
```

And to define and extra publisher (in your class constructor)

```Python
self.pub_odom = rospy.Publisher('/turtle1/odom', Odometry, queue_size=1)
```
