# README #

Here you can find all the labs corresponding to the Probabilistic Robotics (PR) course of the Erasmus Mundus Joint Master Degree in Intelligent Field Robotic Systems (IFROS) and the local Master on Intelligent Robotic Systems (MIRS).

Remember to pull the repo before starting any prelab or lab session.

### Requeriments ###

* [Ubuntu 20.04 LTS](http://releases.ubuntu.com/20.04/)
* [ROS Noetic](http://wiki.ros.org/noetic/Installation/Ubuntu)
* Clone this repo in your catkin workspace and compile it:

```bash
cd ~/catkin_ws/src
git clone https://bitbucket.org/udg_cirs/probabilistic_labs.git
cd ..
catkin_make #or catkin build
```

### Contents ###

* Turtlebot2 introduction: `turtlebot2`
* Split & Merge algorithm for line extraction: `lab2_splitandmerge` package
* Particle Filter (python only): `particle_filter_python`
* Particle Filter (ROS): `lab3_particlefilter`
* Extended Kalman Filter (python only): `kalman_filter_python`
* Extended Kalman Filter (ROS): `lab4_ekf`
* Simultaneous Localization and Mapping (lines): `lab5_slam`

